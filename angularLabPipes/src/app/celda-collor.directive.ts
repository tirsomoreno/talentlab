import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appCeldaCollor]'
})

export class CeldaCollorDirective {

  @HostBinding('class') bgClass: string = '';

  setColor(dark: boolean) {

    this.bgClass = dark ? 'bg-dark' : '';

  }

}
