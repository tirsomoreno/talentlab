import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { StockModule } from './stock/stock.module';
import { AppComponent } from './app.component';
import { IteradorDirective } from './iterador.directive';
import { CeldaCollorDirective } from './celda-collor.directive';



@NgModule({
  declarations: [
    AppComponent, IteradorDirective, CeldaCollorDirective
  ],
  imports: [
    BrowserModule, StockModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
