import { Component, Input, ViewChildren, QueryList, OnInit } from '@angular/core';
import { Repositorio } from '../modelo/repositorio';
import { Producto } from '../modelo/producto';


@Component({
  selector: 'app-tabla-producto',
  templateUrl: './tabla-producto.component.html',
  styleUrls: ['./tabla-producto.component.css']
})
export class TablaProductoComponent implements OnInit {

  @Input() repositorio: Repositorio;

  taxRate: number = 0;
  categoryFilter: string;
  itemCount: number = 3;


  constructor() { }

  ngOnInit(): void {
  }

  getProducto(key: number): Producto {
    return this.repositorio.getProducto(key);
  }

  getProductos(): Producto[] {
    return this.repositorio.getProductos();
  }

  deleteProducto(key: number) {
    this.repositorio.deleteProducto(key);
  }
}
