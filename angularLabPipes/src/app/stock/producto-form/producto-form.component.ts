import { Component, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Producto } from '../modelo/producto';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'productoForm',
  templateUrl: './producto-form.component.html',
  // styleUrls: ["productForm.component.css"], 
  // encapsulation: ViewEncapsulation.Emulated 
})

export class ProductoFormComponent {

  nuevoProducto: Producto = new Producto();

  @Output() nuevoProductoEvent = new EventEmitter<Producto>();


  submitForm(form: any) {

    this.nuevoProductoEvent.emit(this.nuevoProducto);
    this.nuevoProducto = new Producto();
    form.reset();

  }
}
