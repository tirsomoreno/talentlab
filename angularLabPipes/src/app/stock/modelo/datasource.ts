import { Producto } from './producto';

export class Datasource {

private data: Producto[];

constructor() {
    this.data = new Array<Producto>( new Producto(1, 'zapatos', 'zapateria', 75000),
                                    new Producto(2, 'pantalon', 'vestuario', 48000),
                                    new Producto(3, 'camisa', 'vestuario', 39500),
                                    new Producto(4, 'detergente', 'limpieza', 3900),
                                    new Producto(5, 'jamon de pavo', 'cecinas', 8000));
    }

    getData(): Producto[] {
        return this.data;
    }
}

