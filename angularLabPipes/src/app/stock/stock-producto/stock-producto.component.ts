import { Component, OnInit } from '@angular/core';
import { Repositorio } from '../modelo/repositorio';
import { Producto } from '../modelo/producto';

@Component({
  selector: 'app-stock-producto',
  templateUrl: './stock-producto.component.html',
  //styleUrls: ['./stock-producto.component.css']
})
export class StockProductoComponent implements OnInit {

  repositorio: Repositorio = new Repositorio();

  constructor() { }

  ngOnInit(): void {
  }

  addProducto(p: Producto) {
    this.repositorio.saveProducto(p);
  }
}
