import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockProductoComponent } from './stock-producto/stock-producto.component';
import { ProductoFormComponent } from './producto-form/producto-form.component';
import { TablaProductoComponent } from './tabla-producto/tabla-producto.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [StockProductoComponent, ProductoFormComponent, TablaProductoComponent],
  imports: [
    CommonModule, FormsModule
  ],
  exports: [StockProductoComponent]
})
export class StockModule { }
