import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockProductoComponent } from './stock-producto/stock-producto.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [StockProductoComponent],
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule
  ],
  exports: [StockProductoComponent]
})
export class StockModule { }
