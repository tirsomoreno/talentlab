import { FormControl, FormGroup, Validators } from '@angular/forms';

export class ProductFormControl extends FormControl {

    label: string;
    modelProperty: string;

    constructor(label: string, property: string, value: any, validator: any) {

        super(value, validator);

        this.label = label;
        this.modelProperty = property;

    }

    getValidationMessages() {

        const messages: string[] = [];

        if (this.errors) {
            // tslint:disable-next-line:forin
            for (const errorName in this.errors) {
                switch (errorName) {
                    case 'required':
                        messages.push(`Debe ingresar un ${this.label}`);
                        break;
                    case 'minlength':
                        messages.push(`Un ${this.label} debe ser de a lo menos ${this.errors.minlength.requiredLength} caracteres`);
                        break;
                    case 'maxlength':
                        messages.push(`Un ${this.label} no debe ser de mas de ${this.errors.maxlength.requiredLength} caracteres`);
                        break;
                    case 'pattern':
                        messages.push(`El ${this.label} contiene caracteres no permitidos`);
                        break;
                }
            }
        }
        return messages;
    }
}

export class ProductFormGroup extends FormGroup {

    constructor() {
        super({
            nombre: new ProductFormControl('Nombre', 'nombre', '', Validators.required),

            // tslint:disable-next-line:max-line-length
            categoria: new ProductFormControl('Categoria', 'categoria', '', Validators.compose([Validators.required, Validators.pattern('^[A-Za-z ]+$'), Validators.minLength(3), Validators.maxLength(10)])),

            // tslint:disable-next-line:max-line-length
            precio: new ProductFormControl('Precio', 'precio', '', Validators.compose([Validators.required, Validators.pattern('^[0-9\.]+$')]))
        });
    }

    get productControls(): ProductFormControl[] {

        return Object.keys(this.controls).map(k => this.controls[k] as ProductFormControl);
    }

    getValidationMessages(name: string): string[] {

        return (this.controls[name] as ProductFormControl).getValidationMessages();
    }

    getFormValidationMessages(): string[] {

        const messages: string[] = [];

        Object.values(this.controls).forEach(c => messages.push(...(c as ProductFormControl).getValidationMessages()));

        return messages;
    }
}
