import { Producto } from './producto';
import { Datasource } from './datasource';


export class Repositorio {
    private dataSource: Datasource;
    private productos: Producto[];
    private locator = (p: Producto, id: number) => p.id == id;

    constructor() {
        this.dataSource = new Datasource();
        this.productos = new Array<Producto>();
        this.dataSource.getData().forEach(p => this.productos.push(p));
    }

    getProductos(): Producto[] {

        return this.productos;

    }

    getProducto(id: number): Producto {

        return this.productos.find(p => this.locator(p, id));

    }

    saveProducto(producto: Producto) {
        if (producto.id == 0 || producto.id == null) {
            producto.id = this.generateID();
            this.productos.push(producto);
        } else {
            let index = this.productos .findIndex(p => this.locator(p, producto.id)); 
            
            this.productos.splice(index, 1, producto);
        }
    }

    deleteProducto(id: number) {
        let index = this.productos.findIndex(p => this.locator(p, id));

        if (index > -1) {
            this.productos.splice(index, 1);
        }
    }

    private generateID(): number {
        let candidato = 100;

        while (this.getProducto(candidato) != null) {
                candidato++;
            }
        return candidato;
    }
}
