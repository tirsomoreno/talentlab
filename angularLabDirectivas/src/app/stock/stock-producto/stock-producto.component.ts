import { Component, OnInit } from '@angular/core';
import { Repositorio } from '../modelo/repositorio';
import { Producto } from '../modelo/producto';
import { NgForm } from '@angular/forms';
import { ProductFormGroup, ProductFormControl } from '../modelo/form-producto';

@Component({
  selector: 'app-stock-producto',
  templateUrl: './stock-producto.component.html',
  styleUrls: ['./stock-producto.component.css']
})
export class StockProductoComponent implements OnInit {


  constructor() {

    this.repositorio = new Repositorio();

  }

  repositorio: Repositorio;
  nuevoProducto: Producto = new Producto();
  formSubmitted = false;
  formGroup: ProductFormGroup = new ProductFormGroup();


  getProducto(key: number): Producto {
    return this.repositorio.getProducto(key);
  }

  getProductos(): Producto[] {
    return this.repositorio.getProductos();
  }


  get jsonProduct() {
    return JSON.stringify(this.nuevoProducto);
  }

  agregaProducto(p: Producto) {
    this.repositorio.saveProducto(p);
  }

  ngOnInit(): void {
  }

  submitForm() {

    this.agregaProducto(this.nuevoProducto);
  }
}

